import Vue from 'vue'
import Toast from 'vue-easy-toast/dist/vue-easy-toast-lite.min.js'
require('vue-easy-toast/dist/vue-easy-toast.css') // optional

Vue.use(Toast)
